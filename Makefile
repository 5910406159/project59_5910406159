CC = gcc.exe
CXX = g++.exe

CFLAGS = -std=c99 -I/mingw32/include
LDFLAGS =-L/mingw32/lib -lSDLmain -lSDL -lSDL_image -lSDL_mixer -lSDL_ttf -lopengl32 -lglu32

all: Breakout.exe

cp_functions.o: cp_functions.c
	$(CC) $(CFLAGS) -c cp_functions.c

main.o: main.c
	$(CC) $(CFLAGS) -c main.c

Breakout.exe: cp_functions.o main.o
	$(CC) -o Breakout.exe cp_functions.o main.o $(LDFLAGS)

clean: 
	rm -f *.o Breakout.exe