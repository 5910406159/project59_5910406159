//what we doing now
//find how to show life

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cp_functions.h"

#define True  1
#define False 0
#define WindowTitle  "Breakout"
#define WindowWidth  800
#define WindowHeight 700
#define K_SPACE   SDLK_SPACE
#define UP_ARROW   SDLK_UP
#define DOWN_ARROW SDLK_DOWN
#define K_ENTER   SDLK_RETURN

Sound hit_paddle_sound, hit_brick_sound;
Sound hit_top_sound, end_sound;
Music background_music;
Texture paddle_texture, ball_texture,ball2_texture,ball3_texture;
Texture brick_texture, background_texture,brick2_texture,background2_texture,background3_texture;
Texture menu_texture;
Font big_font, small_font;

// Structure for storing info for objects, i.e. Paddle, Brick, Ball.
typedef struct
{
   float x, y;
   float width, height;
   int destroyed;
   float vel_x, vel_y;
} Object;

// Collision Detection between two objects a and b
int collide(Object a, Object b)
{
   if (a.x + a.width  < b.x || b.x + b.width  < a.x ||
       a.y + a.height < b.y || b.y + b.height < a.y)
      return False;
   else
      return True;
}

// Initial routine to load sounds, textures, and fonts.
int game_init()
{
   hit_paddle_sound = cpLoadSound("hitDown.wav");
   hit_brick_sound = cpLoadSound("hitUp.wav");
   hit_top_sound = cpLoadSound("hitTop.wav");
   end_sound = cpLoadSound("theEnd.wav");
   background_music = cpLoadMusic("bgmusic.mp3");


   paddle_texture = cpLoadTexture("Weapon.png");
   ball_texture = cpLoadTexture("ninja.png");
   ball2_texture = cpLoadTexture("ninja2.png");
   ball3_texture = cpLoadTexture("ball3.png");
   brick_texture = cpLoadTexture("pad2.png");
   brick2_texture = cpLoadTexture("brick2.png");
   background_texture = cpLoadTexture("new.png");
   background2_texture = cpLoadTexture("bg2.png");
   background3_texture = cpLoadTexture("bg3.png");
   menu_texture = cpLoadTexture("menu.png");
   big_font = cpLoadFont("THSarabun.ttf", 60);
   small_font = cpLoadFont("THSarabun.ttf", 30);

   if (hit_paddle_sound == NULL || hit_brick_sound == NULL ||
      hit_top_sound == NULL || end_sound == NULL ||
      paddle_texture == NULL || ball_texture == NULL ||
      brick_texture == NULL || background_texture == NULL ||
      big_font == NULL || small_font == NULL ||
      menu_texture== NULL || background2_texture==NULL ||
      ball2_texture == NULL || background3_texture == NULL ||
      ball3_texture == NULL)
      return False;
   return True;
}

//menu interface
void menu(int select){
   Event event;
   int running = True;
   cpDrawTexture(255, 255, 255,  0, 0, WindowWidth, WindowHeight, menu_texture);
   cpDrawText(255, 255, 255, 400, 150, "Start", big_font, 1);
   cpDrawText(255, 255, 255, 400, 350, "Score Board", big_font, 1);
   cpDrawText(255, 255, 255, 400, 550, "Quit", big_font, 1);
   cpSwapBuffers();
   while(running){
      // Draw Background menu
      cpDrawTexture(255, 255, 255,  0, 0, WindowWidth, WindowHeight, menu_texture);
      //cpClearScreen();
      cpDrawText(255, 255, 255, 400, 150, "Start", big_font, 1);
      cpDrawText(255, 255, 255, 400, 350, "Score Board", big_font, 1);
      cpDrawText(255, 255, 255, 400, 550, "Quit", big_font, 1);
      while(cbPollEvent(&event)){
         if(event.type == KEYDOWN){
            if(event.key.keysym.sym == DOWN_ARROW){
               select += 1;
               if(select > 3) 
                  select -= 3;
            }
            if(event.key.keysym.sym == UP_ARROW){
               select -= 1;
               if(select < 1)
                  select += 3;
            }
         }
         if(select == 1) {cpDrawText(0, 255, 255, 400, 150, "Start", big_font, 1);}
         if(select == 2) {cpDrawText(0, 255, 255, 400, 350, "Score Board", big_font, 1);}
         if(select == 3) {cpDrawText(0, 255, 255, 400, 550, "Quit", big_font, 1);}
         if(event.key.keysym.sym == K_ENTER){
            if(select == 1) {stage();}
            if(select == 3) {exit(1);}
         }
         if (event.type == QUIT ||
               event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
               running = False;
               break;
         }
         cpSwapBuffers();
      }
   }
}
/* The main() function is called when the program starts. */
int stage()
{
   int running, n_bricks = 120, n_hits = 0, score = 0,life = 2,start=1,reset=1,count=0;
   int start_ball_vel = 10;
   float paddle_vel_x = 0;
   char msg[80];
   Object bricks[n_bricks];
   Object ball = {WindowWidth/2-12, 350, 24, 24, False, 0, -start_ball_vel};
   Object paddle = {WindowWidth/2-62, WindowHeight-50, 124, 18, False, 7, 0};
   Event event;

   // Window Initialization
   if (cpInit(WindowTitle, WindowWidth, WindowHeight) == False) {
     printf("Window Initialization Failed!\n");
     exit(1);
   }
   // Game Initialization
   if (game_init() == False) {
     printf("Game Initialization Failed!\n");
     exit(1);
   }

   // Brick setup
   for (int n = 0, x = -10, y = 80, flip = 0; n < n_bricks; n++) {
      bricks[n].width = 55;
      bricks[n].height = 18;
      if (x > WindowWidth) {
         x  = -10;
         y += bricks[n].height;
      }
      bricks[n].x = x;
      bricks[n].y = y;
      bricks[n].destroyed = False;
      x += bricks[n].width;
   }

   running = True;
   while (running) {
      cpClearScreen();
      // Draw background
      cpDrawTexture(255, 255, 255,
         0, 0, WindowWidth, WindowHeight, background_texture);
      // Draw paddle
      cpDrawTexture(255, 255, 255,
         paddle.x, paddle.y, paddle.width, paddle.height, paddle_texture);
      // Draw ball
      cpDrawTexture(255, 255, 255,
         ball.x, ball.y, ball.width, ball.height, ball_texture);
      // Display score
      sprintf(msg, "คะแนน %d", score);
      cpDrawText(255, 0, 0, 3, 3, msg, small_font, 0);
      sprintf(msg, "life %d", life);
      cpDrawText(255, 0, 0, 750, 3, msg, small_font, 0);

      // Draw bricks
      for (int n = 0; n < n_bricks; n++) {
            if (!bricks[n].destroyed)
               cpDrawTexture(255, 255, 255,
                  bricks[n].x, bricks[n].y, bricks[n].width, bricks[n].height,
                  brick_texture);
      }

      // If the ball has left the screen or all bricks were destroyed,
      // then the game is over.
      
      if (ball.y + ball.width > WindowHeight) {
         if(life==0){
            cpPlaySound(end_sound);
            cpDrawText(255, 255, 0, 400, 350, "GAME OVER", big_font, 1);
            cpDrawText(255, 255, 0, 400, 400, "Press ENTER to menu", big_font, 1);
            cpSwapBuffers();
            reset = 0;
            while (1) {
               cbPollEvent(&event);
               if(event.type == QUIT ||
                  event.type == KEYUP && event.key.keysym.sym == K_ESCAPE){
                  exit(1);
               }
               if (event.type == KEYUP && event.key.keysym.sym == K_ENTER) {
                  menu(1);
               }
            }
         }
         if(start==0){
            cpDrawText(255, 0, 0, 400, 350, "press Space Bar to continus", big_font, 1);
            if(event.type == KEYUP && event.key.keysym.sym == K_SPACE){
               start++;
            }
         }  
         //decrease life
         else{
            life--;
            ball.x = WindowWidth/2-12;
            ball.y = 350;
            ball.vel_x = 0;
            ball.vel_y = 0;
            start--;
         }
      }
      if(start==0)
         cpDrawText(255, 0, 0, 400, 300, "press Space Bar to continus", big_font, 1);
      //all bricks has been destroy
      if(n_hits == n_bricks){
         cpDrawText(255, 255, 0, 400, 400, "Go to next stage press Space Bar", big_font, 1);
         cpDrawText(255, 255, 0, 400, 300, "Press ENTER to MENU", big_font, 1);

         cpSwapBuffers();
         while (1) {
            cbPollEvent(&event);
            if(event.type == KEYUP && event.key.keysym.sym == K_SPACE){
               secondStage(score);
            }
            if(event.type == KEYUP && event.key.keysym.sym == K_ENTER){
               menu(1);
            }
         }
      }

      // All drawn objects are now displayed.
      cpSwapBuffers();

      // Get an event from event queue.
      while (cbPollEvent(&event)) {
         // If the window is close or Esc key is just released,
         // then the program is about to exit main loop.
         if (event.type == QUIT ||
             event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
            running = False;
            break;
         }
         //start with another life
         if (event.type == KEYDOWN){
            if(event.key.keysym.sym == K_SPACE && start==0){
               ball.vel_y = 10;
               start++;
            }   
         }
         if (event.type == KEYDOWN){
            if(event.key.keysym.sym == K_ENTER && reset==0){
               life = 2;
               reset=1;
               start++;
            }   
         }

         // Paddle left, right control routine
         if (event.type == KEYDOWN) {
            if (event.key.keysym.sym == K_LEFT)
               paddle_vel_x = -paddle.vel_x-5;
            if (event.key.keysym.sym == K_RIGHT)
               paddle_vel_x = paddle.vel_x+5;
         }
         else
         if (event.type == KEYUP) {
            if (event.key.keysym.sym == K_LEFT && paddle_vel_x < 0)
               paddle_vel_x = 0;
            if (event.key.keysym.sym == K_RIGHT && paddle_vel_x > 0)
               paddle_vel_x = 0;
         }
      }
      // Move paddle according to its velocity
      paddle.x += paddle_vel_x;

      // Prevent paddle to not penetrate walls
      if (paddle.x < 0)
         paddle.x = 0;
      if (paddle.x + paddle.width > WindowWidth)
         paddle.x = WindowWidth - paddle.width;
      if (ball.x < 0 || ball.x + ball.width > WindowWidth){
      //ball.vel_x = -ball.vel_x;
      if(ball.x<0){
         if(ball.vel_x>0){
            ball.vel_x=ball.vel_x;
         }
         else{
           ball.vel_x = -ball.vel_x;
         }
      }
      else{
         if(ball.vel_x<0){
            ball.vel_x=ball.vel_x;
         }
         else{
            ball.vel_x = -ball.vel_x;
         } 
      }
   }
      // Move ball according to its velocity
      ball.x += ball.vel_x;
      ball.y += ball.vel_y;

      // Prevent ball to not penetrate walls
      if (ball.x < 0 || ball.x + ball.width > WindowWidth)
         ball.vel_x = -ball.vel_x;

      // If ball hits ceiling, then it is bounced back.
      if (ball.y < 0) {
         cpPlaySound(hit_top_sound);
         ball.vel_y = -ball.vel_y;
      }

      // Bricks-Ball collision check
      for (int n = 0; n < n_bricks; n++) {
         if (!bricks[n].destroyed &&
            collide(ball, bricks[n]) == True) {
            cpPlaySound(hit_brick_sound);
            ball.vel_y = -ball.vel_y;
            bricks[n].destroyed = True;
            n_hits++;
            score += 10;
            break;
         }
      }

      // Paddle-Ball collision check
      if (collide(ball, paddle) == True) {
         cpPlaySound(hit_paddle_sound);
         ball.vel_x = (((ball.x + ball.width/2.1 - paddle.x - paddle.width/2.1)/(paddle.width/2.1))*4);
         //vel_result = sqrt(pow(ball.vel_x,2) + pow(ball.vel_y,2));
         ball.vel_y = -fabs(ball.vel_y);
         
      }
      cpDelay(10);
   }

   cpCleanUp();
   return 0;
}
int secondStage(int score)
{
   int running, n_bricks = 120, n_hits = 0,life = 2,start=1,reset=1,count=0;
   int start_ball_vel = 12.5;
   float paddle_vel_x = 0;
   char msg[80];
   Object bricks[n_bricks];
   Object ball = {WindowWidth/2-12, 350, 24, 24, False, 0, -start_ball_vel};
   Object paddle = {WindowWidth/2-62, WindowHeight-50, 124, 18, False, 7, 0};
   Event event;

      // Window Initialization
   if (cpInit(WindowTitle, WindowWidth, WindowHeight) == False) {
     printf("Window Initialization Failed!\n");
     exit(1);
   }
   // Game Initialization
   if (game_init() == False) {
     printf("Game Initialization Failed!\n");
     exit(1);
   }

   // Brick setup
   for (int n = 0, x = -10, y = 80, flip = 0; n < n_bricks; n++) {
      bricks[n].width = 55;
      bricks[n].height = 18;
      if (x > WindowWidth) {
         x  = -10;
         y += bricks[n].height;
      }
      bricks[n].x = x;
      bricks[n].y = y;
      bricks[n].destroyed = False;
      x += bricks[n].width;
   }

   running = True;
   while (running) {
      cpClearScreen();
      // Draw background
      cpDrawTexture(255, 255, 255,
         0, 0, WindowWidth, WindowHeight, background2_texture);
      // Draw paddle
      cpDrawTexture(255, 255, 255,
         paddle.x, paddle.y, paddle.width, paddle.height, paddle_texture);
      // Draw ball
      cpDrawTexture(255, 255, 255,
         ball.x, ball.y, ball.width, ball.height, ball2_texture);
      // Display score
      sprintf(msg, "คะแนน %d", score);
      cpDrawText(255, 255, 255, 3, 3, msg, small_font, 0);
      sprintf(msg, "life %d", life);
      cpDrawText(255, 255, 255, 750, 3, msg, small_font, 0);

      // Draw bricks
      for (int n = 0; n < n_bricks; n++) {
            if (!bricks[n].destroyed)
               cpDrawTexture(255, 255, 255,
                  bricks[n].x, bricks[n].y, bricks[n].width, bricks[n].height,
                  brick_texture);
      }

      // If the ball has left the screen or all bricks were destroyed,
      // then the game is over.
      
      if (ball.y + ball.width > WindowHeight) {
         if(life==0){
            cpPlaySound(end_sound);
            cpDrawText(255, 255, 0, 400, 350, "GAME OVER", big_font, 1);
            cpDrawText(255, 255, 0, 400, 400, "Press ENTER to menu", big_font, 1);
            cpSwapBuffers();
            reset = 0;
            while (1) {
               cbPollEvent(&event);
               if(event.type == QUIT ||
                  event.type == KEYUP && event.key.keysym.sym == K_ESCAPE){
                  exit(1);
               }
               if (event.type == KEYUP && event.key.keysym.sym == K_ENTER) {
                  menu(1);
               }
            }
         }
         if(start==0){
            cpDrawText(255, 255, 0, 400, 350, "press Space Bar to continus", big_font, 1);
            if(event.type == KEYUP && event.key.keysym.sym == K_SPACE){
               start++;
            }
         }  
         //decrease life
         else{
            life--;
            ball.x = WindowWidth/2-12;
            ball.y = 350;
            ball.vel_x = 0;
            ball.vel_y = 0;
            start--;
         }
      }
      if(start==0)
         cpDrawText(255, 255, 0, 400, 300, "press Space Bar to continus", big_font, 1);
      //all bricks has been destroy
      if(n_hits == n_bricks){
         cpDrawText(255, 255, 0, 400, 400, "Go to next stage press Space Bar", big_font, 1);
         cpDrawText(255, 255, 0, 400, 300, "Press ENTER to MENU", big_font, 1);

         cpSwapBuffers();
         while (1) {
            cbPollEvent(&event);
            if(event.type == KEYUP && event.key.keysym.sym == K_SPACE){
               thirdStage(score);
            }
            if(event.type == KEYUP && event.key.keysym.sym == K_ENTER){
               menu(1);
            }
         }
      }

      // All drawn objects are now displayed.
      cpSwapBuffers();

      // Get an event from event queue.
      while (cbPollEvent(&event)) {
         // If the window is close or Esc key is just released,
         // then the program is about to exit main loop.
         if (event.type == QUIT ||
             event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
            running = False;
            break;
         }
         //start with another life
         if (event.type == KEYDOWN){
            if(event.key.keysym.sym == K_SPACE && start==0){
               ball.vel_y = 10;
               start++;
            }   
         }
         if (event.type == KEYDOWN){
            if(event.key.keysym.sym == K_ENTER && reset==0){
               life = 2;
               reset=1;
               start++;
            }   
         }

         // Paddle left, right control routine
         if (event.type == KEYDOWN) {
            if (event.key.keysym.sym == K_LEFT)
               paddle_vel_x = -paddle.vel_x-5;
            if (event.key.keysym.sym == K_RIGHT)
               paddle_vel_x = paddle.vel_x+5;
         }
         else
         if (event.type == KEYUP) {
            if (event.key.keysym.sym == K_LEFT && paddle_vel_x < 0)
               paddle_vel_x = 0;
            if (event.key.keysym.sym == K_RIGHT && paddle_vel_x > 0)
               paddle_vel_x = 0;
         }
      }
      // Move paddle according to its velocity
      paddle.x += paddle_vel_x;

      // Prevent paddle to not penetrate walls
      if (paddle.x < 0)
         paddle.x = 0;
      if (paddle.x + paddle.width > WindowWidth)
         paddle.x = WindowWidth - paddle.width;
      if (ball.x < 0 || ball.x + ball.width > WindowWidth){
      //ball.vel_x = -ball.vel_x;
      if(ball.x<0){
         if(ball.vel_x>0){
            ball.vel_x=ball.vel_x;
         }
         else{
           ball.vel_x = -ball.vel_x;
         }
      }
      else{
         if(ball.vel_x<0){
            ball.vel_x=ball.vel_x;
         }
         else{
            ball.vel_x = -ball.vel_x;
         } 
      }
   }
      // Move ball according to its velocity
      ball.x += ball.vel_x*1.025;
      ball.y += ball.vel_y*1.025;

      // Prevent ball to not penetrate walls
      if (ball.x < 0 || ball.x + ball.width > WindowWidth)
         ball.vel_x = -ball.vel_x;

      // If ball hits ceiling, then it is bounced back.
      if (ball.y < 0) {
         cpPlaySound(hit_top_sound);
         ball.vel_y = -ball.vel_y;
      }

      // Bricks-Ball collision check
      for (int n = 0; n < n_bricks; n++) {
         if (!bricks[n].destroyed &&
            collide(ball, bricks[n]) == True) {
            cpPlaySound(hit_brick_sound);
            ball.vel_y = -ball.vel_y;
            bricks[n].destroyed = True;
            n_hits++;
            score += 10;
            break;
         }
      }

      // Paddle-Ball collision check
      if (collide(ball, paddle) == True) {
         cpPlaySound(hit_paddle_sound);
         ball.vel_x = (((ball.x + ball.width/2.1 - paddle.x - paddle.width/2.1)/(paddle.width/2.1))*4)*1.0025;
         //vel_result = sqrt(pow(ball.vel_x,2) + pow(ball.vel_y,2));
         ball.vel_y = -fabs(ball.vel_y*1.0025);
         
      }
      cpDelay(10);
   }

   cpCleanUp();
   return 0;
}
int thirdStage(int score)
{
   int running, n_bricks = 120, n_hits = 0,life = 2,start=1,reset=1,count=0;
   int start_ball_vel = 15;
   float paddle_vel_x = 0;
   char msg[80];
   Object bricks[n_bricks];
   Object ball = {WindowWidth/2-12, 350, 24, 24, False, 0, -start_ball_vel};
   Object paddle = {WindowWidth/2-62, WindowHeight-50, 124, 18, False, 7, 0};
   Event event;

      // Window Initialization
   if (cpInit(WindowTitle, WindowWidth, WindowHeight) == False) {
     printf("Window Initialization Failed!\n");
     exit(1);
   }
   // Game Initialization
   if (game_init() == False) {
     printf("Game Initialization Failed!\n");
     exit(1);
   }

   // Brick setup
   for (int n = 0, x = -10, y = 80, flip = 0; n < n_bricks; n++) {
      bricks[n].width = 55;
      bricks[n].height = 18;
      if (x > WindowWidth) {
         x  = -10;
         y += bricks[n].height;
      }
      bricks[n].x = x;
      bricks[n].y = y;
      bricks[n].destroyed = False;
      x += bricks[n].width;
   }

   running = True;
   while (running) {
      cpClearScreen();
      // Draw background
      cpDrawTexture(255, 255, 255,
         0, 0, WindowWidth, WindowHeight, background3_texture);
      // Draw paddle
      cpDrawTexture(255, 255, 255,
         paddle.x, paddle.y, paddle.width, paddle.height, paddle_texture);
      // Draw ball
      cpDrawTexture(255, 255, 255,
         ball.x, ball.y, ball.width, ball.height, ball3_texture);
      // Display score
      sprintf(msg, "คะแนน %d", score);
      cpDrawText(255, 255, 255, 3, 3, msg, small_font, 0);
      sprintf(msg, "life %d", life);
      cpDrawText(255, 255, 255, 750, 3, msg, small_font, 0);

      // Draw bricks
      for (int n = 0; n < n_bricks; n++) {
            if (!bricks[n].destroyed)
               cpDrawTexture(255, 255, 255,
                  bricks[n].x, bricks[n].y, bricks[n].width, bricks[n].height,
                  brick_texture);
      }

      // If the ball has left the screen or all bricks were destroyed,
      // then the game is over.
      
      if (ball.y + ball.width > WindowHeight) {
         if(life==0){
            cpPlaySound(end_sound);
            cpDrawText(255, 255, 0, 400, 350, "GAME OVER", big_font, 1);
            cpDrawText(255, 255, 0, 400, 400, "Press ENTER to menu", big_font, 1);
            cpSwapBuffers();
            reset = 0;
            while (1) {
               if(event.type == QUIT ||
                  event.type == KEYUP && event.key.keysym.sym == K_ESCAPE){
                  exit(1);
               }
               cbPollEvent(&event);
               if (event.type == KEYUP && event.key.keysym.sym == K_ENTER) {
                  menu(1);
               }
            }
         }
         if(start==0){
            cpDrawText(255, 255, 0, 400, 350, "press Space Bar to continus", big_font, 1);
            if(event.type == KEYUP && event.key.keysym.sym == K_SPACE){
               start++;
            }
         }  
         //decrease life
         else{
            life--;
            ball.x = WindowWidth/2-12;
            ball.y = 350;
            ball.vel_x = 0;
            ball.vel_y = 0;
            start--;
         }
      }
      if(start==0)
         cpDrawText(255, 255, 0, 400, 300, "press Space Bar to continus", big_font, 1);
      //all bricks has been destroy
      if(n_hits == n_bricks){
         cpDrawText(255, 255, 0, 400, 400, "CONGRATULATION", big_font, 1);
         cpDrawText(255, 255, 0, 400, 300, "Press ENTER to MENU", big_font, 1);

         cpSwapBuffers();
         while (1) {
            cbPollEvent(&event);
            if(event.type == KEYUP && event.key.keysym.sym == K_ENTER){
               menu(1);
            }
         }
      }

      // All drawn objects are now displayed.
      cpSwapBuffers();

      // Get an event from event queue.
      while (cbPollEvent(&event)) {
         // If the window is close or Esc key is just released,
         // then the program is about to exit main loop.
         if (event.type == QUIT ||
             event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
            running = False;
            break;
         }
         //start with another life
         if (event.type == KEYDOWN){
            if(event.key.keysym.sym == K_SPACE && start==0){
               ball.vel_y = 10;
               start++;
            }   
         }
         if (event.type == KEYDOWN){
            if(event.key.keysym.sym == K_ENTER && reset==0){
               life = 2;
               reset=1;
               start++;
            }   
         }

         // Paddle left, right control routine
         if (event.type == KEYDOWN) {
            if (event.key.keysym.sym == K_LEFT)
               paddle_vel_x = -paddle.vel_x-10;
            if (event.key.keysym.sym == K_RIGHT)
               paddle_vel_x = paddle.vel_x+10;
         }
         else
         if (event.type == KEYUP) {
            if (event.key.keysym.sym == K_LEFT && paddle_vel_x < 0)
               paddle_vel_x = 0;
            if (event.key.keysym.sym == K_RIGHT && paddle_vel_x > 0)
               paddle_vel_x = 0;
         }
      }
      // Move paddle according to its velocity
      paddle.x += paddle_vel_x;

      // Prevent paddle to not penetrate walls
      if (paddle.x < 0)
         paddle.x = 0;
      if (paddle.x + paddle.width > WindowWidth)
         paddle.x = WindowWidth - paddle.width;
      if (ball.x < 0 || ball.x + ball.width > WindowWidth){
      //ball.vel_x = -ball.vel_x;
      if(ball.x<0){
         if(ball.vel_x>0){
            ball.vel_x=ball.vel_x;
         }
         else{
           ball.vel_x = -ball.vel_x;
         }
      }
      else{
         if(ball.vel_x<0){
            ball.vel_x=ball.vel_x;
         }
         else{
            ball.vel_x = -ball.vel_x;
         } 
      }
   }
      // Move ball according to its velocity
      ball.x += ball.vel_x*1.025;
      ball.y += ball.vel_y*1.025;

      // Prevent ball to not penetrate walls
      if (ball.x < 0 || ball.x + ball.width > WindowWidth)
         ball.vel_x = -ball.vel_x;

      // If ball hits ceiling, then it is bounced back.
      if (ball.y < 0) {
         cpPlaySound(hit_top_sound);
         ball.vel_y = -ball.vel_y;
      }

      // Bricks-Ball collision check
      for (int n = 0; n < n_bricks; n++) {
         if (!bricks[n].destroyed &&
            collide(ball, bricks[n]) == True) {
            cpPlaySound(hit_brick_sound);
            ball.vel_y = -ball.vel_y;
            bricks[n].destroyed = True;
            n_hits++;
            score += 10;
            break;
         }
      }

      // Paddle-Ball collision check
      if (collide(ball, paddle) == True) {
         cpPlaySound(hit_paddle_sound);
         ball.vel_x = (((ball.x + ball.width/2.1 - paddle.x - paddle.width/2.1)/(paddle.width/2.1))*4)*1.0025;
         //vel_result = sqrt(pow(ball.vel_x,2) + pow(ball.vel_y,2));
         ball.vel_y = -fabs(ball.vel_y*1.0025);
         
      }
      cpDelay(10);
   }

   cpCleanUp();
   return 0;
}
int main()
{
   if (cpInit(WindowTitle, WindowWidth, WindowHeight) == False) {
     printf("Window Initialization Failed!\n");
     exit(1);
   }
   // Game Initialization
   if (game_init() == False) {
     printf("Game Initialization Failed!\n");
     exit(1);
   }
   Mix_PlayMusic(background_music, -1);
   Mix_VolumeMusic(70);
   menu(1);
   cpCleanUp();
   return 0;
}
